﻿using EasyFlow;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            SpeedTest();
            //CompositeTest();

            Console.ReadKey();
        }

        private static void CompositeTest()
        {
            var activityProvider = new ActivityFactory();
            var composite = new FlowModule(activityProvider);

            var sum = composite.AddActivity<SumActivity>();

            var compositeInput1 = composite.AddChannelToTheOutside(sum.In1Channel, "In1");
            var compositeInput2 = composite.AddChannelToTheOutside(sum.In2Channel, "In2");
            var compositeOutput = composite.AddChannelToTheOutside(sum.OutChannel, "Out");

            var workflow = new Workflow(activityProvider);
            workflow.AddActivity(composite);
            var sensor1 = workflow.AddActivity<SensorActivity>();
            var sensor2 = workflow.AddActivity<SensorActivity>();
            var absolute = workflow.AddActivity<AbsoluteActivity>();

            sensor1.OutChannel.ConnectTo(compositeInput1);
            sensor2.OutChannel.ConnectTo(compositeInput2);
            compositeOutput.ConnectTo(absolute.InChannel);

            workflow.Execute();
        }

        private static void SpeedTest()
        {
            var activityProvider = new ActivityFactory();
            var workflow = new Workflow(activityProvider);

            var sensor1 = workflow.AddActivity<SensorActivity>();
            var sensor2 = workflow.AddActivity<SensorActivity>();
            var subtract = workflow.AddActivity<SubtractActivity>();
            var absolute = workflow.AddActivity<AbsoluteActivity>();
            var constant = workflow.AddActivity<DoubleConstantActivity>(a => a.Value = 12);
            var compare = workflow.AddActivity<CompareActivity>(a => a.CompareType = CompareType.Larger);
            var alarm = workflow.AddActivity<AlarmActivity>();
            var consoleOutput = workflow.AddActivity<ConsoleActivity>();

            sensor1.OutChannel.ConnectTo(subtract.In1Channel);
            sensor2.OutChannel.ConnectTo(subtract.In2Channel);
            subtract.OutChannel.ConnectTo(absolute.InChannel);
            absolute.OutChannel.ConnectTo(compare.InValueChannel);
            constant.OutChannel.ConnectTo(compare.InBaseValueChannel);
            compare.OutChannel.ConnectTo(alarm.InChannel);
            // Uncomment this line to output the result to the console
            //compare.OutChannel.ConnectTo(consoleOutput.InputChannel);

            Stopwatch clock = new Stopwatch();
            clock.Start();
            int cnt = 0;

            while (true)
            {
                workflow.Execute();
                cnt++;

                if (clock.ElapsedMilliseconds >= 1000)
                {
                    Console.WriteLine(cnt + "/s");
                    cnt = 0;
                    clock.Restart();

                    if (Console.KeyAvailable)
                        break;
                }
            }
        }
    }

    class SensorActivity : FlowActivity
    {
        private Random _random = new Random();

        public bool Trigger { get; set; } = true;
        public FlowOutputChannel<double?> OutChannel { get; }

        public SensorActivity()
        {
            OutChannel = InternalAddOutputChannel<double?>("Out");
        }

        protected override void Execute()
        {
            if (Trigger)
                OutChannel.SetValue(_random.Next(50));
        }
    }

    class ConsoleActivity : FlowActivity
    {
        public FlowInputChannel<object> InputChannel;

        public ConsoleActivity()
        {
            InputChannel = InternalAddInputChannel<object>("Input");
        }

        protected override void Execute()
        {
            if (InputChannel.Value != null)
                Console.WriteLine(InputChannel.Value.ToString());
        }
    }

    class AlarmActivity : FlowActivity
    {
        public FlowInputChannel<bool?> InChannel;

        public AlarmActivity()
        {
            InChannel = InternalAddInputChannel<bool?>("In");
        }

        protected override void Execute()
        {
            if (InChannel.Value == true)
            {
                // TODO: trigger alarm if true
            }
        }
    }
}
