﻿using EasyFlow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleTest
{
    public class ActivityFactory : IActivityFactory
    {
        public FlowActivity Create(Type activityType)
        {
            return (FlowActivity)Activator.CreateInstance(activityType);
        }
    }
}
