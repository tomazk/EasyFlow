﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow
{
  /// <summary>
  /// This factory is used to create activities when they are added to the module by type. The factory is used
  /// in order tu support resolving the activities using DI container.
  /// </summary>
  public interface IActivityFactory
  {
    FlowActivity Create(Type activityType);
  }
}
