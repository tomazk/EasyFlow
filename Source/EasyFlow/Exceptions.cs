﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyFlow
{
    public class FlowException : Exception
    {
        public FlowException(string message)
            : base(message)
        {
        }
    }

    public class InputChannelConnectionCountException : FlowException
    {
        public InputChannelConnectionCountException()
            : base("Input channel can have only one input connection")
            {
            }
    }

    public class ConnectingChannelsOfSameActivityException : FlowException
    {
        public ConnectingChannelsOfSameActivityException()
            : base("Cannot connect channels that are owned by the same activity")
        {
        }
    }

    public class ConnectingActivitysInDifferentModuleException : FlowException
    {
        public ConnectingActivitysInDifferentModuleException()
            : base("Only two activities in the same module can be connected together")
        {
        }
    }

    public class ActivityAlreadyBelongsToAnotherModuleException : FlowException
    {
        public ActivityAlreadyBelongsToAnotherModuleException()
            : base("Activity already belongs to another module")
        {
        }
    }

    public class ActivityIsNotPlacedInModuleException : FlowException
    {
        public ActivityIsNotPlacedInModuleException()
            : base("Activity is not placed in a module.    ")
        {
        }
    }

    public class OutputChannelvalueAlreadysetException : FlowException
    {
        public OutputChannelvalueAlreadysetException(FlowOutputChannel channel)
            : base($"Value of the output channel '{channel.Name}' was already set")
        {
        }
    }

    public class ChannelValueTypeMustBeNullableException : FlowException
    {
        public ChannelValueTypeMustBeNullableException(string channelName)
            : base($"Channel value type must be a nullable type ({channelName})")
        {
        }

    }

    public class ChannelNameNotUniqueException : Exception
    {
        public ChannelNameNotUniqueException()
            : base("Activities cannot contain two channels with the same name")
        {
        }
    }
}
