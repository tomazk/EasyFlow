﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasyFlow
{
  public sealed class FlowOutputChannel<T> : FlowOutputChannel
  {
    private T? value;

    internal FlowOutputChannel(string name, FlowActivity owner)
        : base(name, typeof(T), owner)
    {
    }

    public void SetValue(T value)
    {
      this.value = value;
    }
    public T? GetValue()
    {
      return value;
    }

    public override void SetValueAsObject(object? value)
    {
      this.value = (value != null && value.GetType() == typeof(T)) ? (T)value : default(T);
    }
    public override void ResetValue()
    {
      value = default(T);
    }
    public override object? GetValueAsObject()
    {
      return this.value;
    }
  }

  internal sealed class FlowOutputChannelWithType : FlowOutputChannel
  {
    private object? value;

    internal FlowOutputChannelWithType(string name, Type type, FlowActivity owner)
        : base(name, type, owner)
    {
    }

    public void SetValue(object? value)
    {
      this.value = value;
    }
    public object? GetValue()
    {
      return value;
    }

    public override void SetValueAsObject(object? value)
    {
      this.value = value;
    }
    public override void ResetValue()
    {
      value = null;
    }
    public override object? GetValueAsObject()
    {
      return this.value;
    }
  }

  public abstract class FlowOutputChannel : FlowChannel
  {
    private LinkedList<FlowInputChannel> connectedInputChannels = new LinkedList<FlowInputChannel>();

    public bool HasConnections => connectedInputChannels.Any();
    public IEnumerable<FlowInputChannel> ConnectedInputChannels => connectedInputChannels;

    internal FlowOutputChannel(string name, Type valueType, FlowActivity owner)
        : base(name, valueType, owner)
    {
      // Check that value type is nullable
      {
        if (valueType.IsValueType && Nullable.GetUnderlyingType(valueType) == null)
        {
          throw new ChannelValueTypeMustBeNullableException(owner.GetType().Name + "." + name);
        }
      }
    }

    public abstract void SetValueAsObject(object? value);
    public abstract void ResetValue();
    public abstract object? GetValueAsObject();

    /// <summary>
    /// Output channel can be connected to the input channel. This means that all the data sent to the output channel of one activity is received on the
    /// input channel of the other activity. By connecting channels together, a pipeline is constructed which can be linear or branched. This is
    /// basically like functional programming where output of one function is an input of the next one.
    /// </summary>
    // TODO: type safety is missing
    public void ConnectTo(FlowInputChannel inputChannel)
    {
      lock (this)
      {
        if (inputChannel == null)
          throw new ArgumentNullException(nameof(inputChannel));

        if (inputChannel.ConnectedOutputChannel != null)
          throw new InputChannelConnectionCountException();

        if (!inputChannel.ValueType.IsAssignableFrom(this.ValueType))
          throw new Exception($"Channel value type missmatch. Output = {Owner.GetType().Name}.{Name} ({ValueType.Name}), input = {inputChannel.Owner.GetType().Name}.{inputChannel.Name} ({inputChannel.ValueType.Name})");

        if (connectedInputChannels.Contains(inputChannel))
          return;

        inputChannel.ConnectedOutputChannel = this;

        connectedInputChannels.AddLast(inputChannel);
      }
    }

    /// <summary>
    /// Disconnects all connected input channels from this output channel.
    /// </summary>
    internal void DisconnectAll()
    {
      lock (this)
      {
        foreach (var channel in connectedInputChannels)
        {
          channel.ConnectedOutputChannel = null;
        }

        connectedInputChannels.Clear();
      }
    }

    /// <summary>
    /// Disconnect from a specific input channel
    /// </summary>
    public void DisconnectFrom(FlowInputChannel inputChannel)
    {
      lock (this)
      {
        if (connectedInputChannels.Contains(inputChannel))
        {
          connectedInputChannels.Remove(inputChannel);

          if (inputChannel.ConnectedOutputChannel == this)
            inputChannel.ConnectedOutputChannel = null;
        }
      }
    }
  }
}
