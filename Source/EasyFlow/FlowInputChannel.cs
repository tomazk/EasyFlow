﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyFlow
{
  public sealed class FlowInputChannel<T> : FlowInputChannel
  {
    internal FlowInputChannel(string name, FlowActivity owner)
        : base(name, typeof(T), owner)
    {
    }

    public T? Value
    {
      get { return GetValue(); }
    }

    private T? GetValue()
    {
      var typedConnectedOutputChannel = ConnectedOutputChannel as FlowOutputChannel<T>;

      if (typedConnectedOutputChannel == null)
      {
        var valueAsObject = ConnectedOutputChannel?.GetValueAsObject();

        return valueAsObject != null ? (T)valueAsObject : default(T);
      }
      else
        return typedConnectedOutputChannel.GetValue();
    }
  }

  internal sealed class FlowInputChannelWithType : FlowInputChannel
  {
    internal FlowInputChannelWithType(string name, Type type, FlowActivity owner)
        : base(name, type, owner)
    {
    }
  }

  public class FlowInputChannel : FlowChannel
  {
    internal FlowInputChannel(string name, Type valueType, FlowActivity owner)
        : base(name, valueType, owner)
    {
      // Check that value type is nullable
      if (valueType.IsValueType && Nullable.GetUnderlyingType(valueType) == null)
        throw new ChannelValueTypeMustBeNullableException(owner.GetType().Name + "." + name);
    }

    /// <summary>
    /// A raw value as an object as it was received.
    /// </summary>
    public object? ValueAsObject
    {
      get { return ConnectedOutputChannel?.GetValueAsObject(); }
    }

    public FlowOutputChannel? ConnectedOutputChannel { get; internal set; }
  }
}
