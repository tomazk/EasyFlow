﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow
{
  public abstract class FlowActivity
  {
    private List<FlowInputChannel> inputChannels = new List<FlowInputChannel>();
    private List<FlowOutputChannel> outputChannels = new List<FlowOutputChannel>();

    public string? Name { get; set; }
    /// <summary>
    /// Unique Id of the activity. Should never be set manually, except when creating it from workflow description.
    /// </summary>
    public Guid ActivityId { get; set; } = Guid.NewGuid();
    public IEnumerable<FlowInputChannel> InputChannels => inputChannels;
    public IEnumerable<FlowOutputChannel> OutputChannels => outputChannels;
    /// <summary>
    /// Whenever an activity finishes processing, a signal is sent also to the <see cref="FinishedChannel"/>
    /// </summary>
    public FlowOutputChannel<ChannelSignal?> FinishedChannel { get; }
    /// <summary>
    /// Whenever an activity encounters an error during processing, a signal is sent to <see cref="ErrorChannel"/>
    /// </summary>
    public FlowOutputChannel<ActivityExceptionInfo?> ErrorChannel { get; }
    
    public FlowActivity()
    {
      // Add two built-in channels
      FinishedChannel = InternalAddOutputChannel<ChannelSignal?>("Finished");
      ErrorChannel = InternalAddOutputChannel<ActivityExceptionInfo?>("Error");
    }

    protected FlowOutputChannel<T> InternalAddOutputChannel<T>(string name)
    {
      var channel = new FlowOutputChannel<T>(name, this);
      outputChannels.Add(channel);
      OnChannelsChanged(channel, ChannelChangeType.Added);
      return channel;
    }
    protected FlowOutputChannel InternalAddOutputChannel(string name, Type valueType)
    {
      var channel = new FlowOutputChannelWithType(name, valueType, this);
      outputChannels.Add(channel);
      OnChannelsChanged(channel, ChannelChangeType.Added);
      return channel;
    }
    protected FlowInputChannel<T> InternalAddInputChannel<T>(string name)
    {
      var channel = new FlowInputChannel<T>(name, this);
      inputChannels.Add(channel);
      OnChannelsChanged(channel, ChannelChangeType.Added);
      return channel;
    }
    protected FlowInputChannel InternalAddInputChannel(string name, Type valueType)
    {
      var channel = new FlowInputChannel(name, valueType, this);
      inputChannels.Add(channel);
      OnChannelsChanged(channel, ChannelChangeType.Added);
      return channel;
    }

    /// <summary>
    /// Connect an output channel to an input channel. Both channels must be of the same type, or the data type from the output channel
    /// must at least be assignable to data type of an input channel.
    /// </summary>
    public void ConnectTo(string outputChannelName, FlowActivity inputActivity, string inputChannelName)
    {
      var outChannel = outputChannels.SingleOrDefault(x => x.Name.Equals(outputChannelName, StringComparison.OrdinalIgnoreCase));

      if (outChannel == null)
        throw new Exception($"Output channel with name {outputChannelName} could not be found on activity {GetType().Name}.");

      var inChannel = inputActivity.inputChannels.SingleOrDefault(x => x.Name.Equals(inputChannelName, StringComparison.OrdinalIgnoreCase));

      if (inChannel == null)
        throw new Exception($"Input channel with name {inputChannelName} could not be found on activity {inputActivity.GetType().Name}.");

      outChannel.ConnectTo(inChannel);
    }

    internal void ExecuteInternal()
    {
      try
      {
        Execute();
      }
      catch (Exception ex)
      {
        if (ErrorChannel.HasConnections)
          ErrorChannel.SetValue(new ActivityExceptionInfo(ActivityId, ex));
        else
          throw; // If activity error handler is not set
      }
    }

    /// <summary>
    /// Called by the workflow to process the input data of this activity and produce an output.
    /// </summary>
    protected abstract void Execute();

    private void OnChannelsChanged(FlowChannel channel, ChannelChangeType changeType)
    {
      ChannelsChanged?.Invoke(this, new ChannelChangedEventArgs(channel, changeType));
    }

    /// <summary>
    /// Trigerred when channels change.
    /// </summary>
    public event EventHandler<ChannelChangedEventArgs>? ChannelsChanged;
  }

  public class ChannelChangedEventArgs : EventArgs
  {
    public FlowChannel Channel { get; }
    public ChannelChangeType ChangeType { get; }

    public ChannelChangedEventArgs(FlowChannel channel, ChannelChangeType changeType)
    {
      Channel = channel;
      ChangeType = changeType;
    }
  }

  public enum ChannelChangeType { Added, Removed }

  public class ChannelSignal
  {
    private static ChannelSignal channelSignal = new ChannelSignal();

    private ChannelSignal()
    {
    }

    public static ChannelSignal Value => channelSignal;
  }

  public class ActivityExceptionInfo
  {
    public Guid Activityid { get; }
    public Exception Exception { get; }

    public ActivityExceptionInfo(Guid activityId, Exception exception)
    {
      this.Activityid = activityId;
      this.Exception = exception;
    }
  }
}
