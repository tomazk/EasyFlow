﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EasyFlow
{
  public class Workflow
  {
    private FlowModule module;

    public Workflow(IActivityFactory activityFactory)
    {
      module = new FlowModule(activityFactory);
    }

    public IEnumerable<FlowActivity> Activities => module.Activities;
    /// <summary>
    /// Unique Id of the workflow. Should never be set manually, except when creating it from workflow description.
    /// </summary>
    public Guid WorkflowId { get; set; } = Guid.NewGuid();

    public T AddActivity<T>(Action<T>? setup = null) where T : FlowActivity
    {
      return (T)AddActivity(typeof(T), a => setup?.Invoke((T)a));
    }
    public FlowActivity AddActivity(Type activityType, Action<FlowActivity>? setup = null)
    {
      return module.AddActivity(activityType, setup);
    }

    public void Execute()
    {
      module.ExecuteModule();
    }
  }
}
