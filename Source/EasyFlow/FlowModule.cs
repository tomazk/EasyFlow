﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow
{
  public sealed class FlowModule : FlowActivity
  {
    private List<FlowActivity> activities = new List<FlowActivity>();
    private InnerInputActivity innerInputActivity = new InnerInputActivity();
    private InnerOutputActivity innerOutputActivity = new InnerOutputActivity();
    // Cached items to speed-up execution
    private List<FlowActivity> cachedExecutionActivities = new List<FlowActivity>();
    private List<FlowOutputChannel> cachedOutputChannels = new List<FlowOutputChannel>();
    private IActivityFactory activityFactory;
    private bool mustRecreateExecutionOrder = true;

    public IEnumerable<FlowActivity> Activities => activities;

    public FlowModule(IActivityFactory activityFactory)
    {
      this.activityFactory = activityFactory;

      AddActivityInternal(innerInputActivity);
      AddActivityInternal(innerOutputActivity);
    }

    public T AddActivity<T>(Action<T>? setup = null) where T: FlowActivity
    {
      return (T)AddActivity(typeof(T), a => setup?.Invoke((T)a));
    }

    public FlowActivity AddActivity(Type activityType, Action<FlowActivity>? setup = null)
    {
      var activity = activityFactory.Create(activityType) as FlowActivity;

      if (activity == null)
        throw new Exception($"Activity does not derive from {nameof(FlowActivity)} class");

      setup?.Invoke(activity);
      activities.Add(activity);
      mustRecreateExecutionOrder = true;

      return activity;
    }

    internal void AddActivityInternal(FlowActivity activity)
    {
      if (!activities.Contains(activity))
      {
        activities.Add(activity);
        mustRecreateExecutionOrder = true;
      }
    }

    /// <summary>
    /// Takes an input channel of an inner activity, creates a new input channel of the same type on the outside
    /// of the module and connects the two channels internally to transfer the values between them.
    /// </summary>
    public FlowInputChannel ExposeChannel(FlowInputChannel innerInputChannel, string channelName)
    {
      FlowInputChannel outerInputChannel = InternalAddInputChannel(channelName, innerInputChannel.ValueType);
      innerInputActivity.BridgeOuterToInnerChannel(innerInputChannel, outerInputChannel);
      return outerInputChannel;
    }
    public FlowInputChannel<T> ExposeChannel<T>(FlowInputChannel<T> innerInputChannel, string channelName)
    {
      FlowInputChannel<T> outerInputChannel = InternalAddInputChannel<T>(channelName);
      innerInputActivity.BridgeOuterToInnerChannel(innerInputChannel, outerInputChannel);
      return outerInputChannel;
    }
    /// <summary>
    /// Takes an output channel of an inner activity, creates a new output channel of the same type on the outside
    /// of the module and connects the two channels internally to transfer the values between them.
    /// </summary>
    public FlowOutputChannel ExposeChannel(FlowOutputChannel innerOutputChannel, string channelName)
    {
      FlowOutputChannel outerOutputChannel = InternalAddOutputChannel(channelName, innerOutputChannel.ValueType);
      innerOutputActivity.BridgeInnerToOuterChannel(innerOutputChannel, outerOutputChannel);
      return outerOutputChannel;
    }
    public FlowOutputChannel<T> ExposeChannel<T>(FlowOutputChannel<T> innerOutputChannel, string channelName)
    {
      FlowOutputChannel<T> outerOutputChannel = InternalAddOutputChannel<T>(channelName);
      innerOutputActivity.BridgeInnerToOuterChannel(innerOutputChannel, outerOutputChannel);
      return outerOutputChannel;
    }

    public void ExecuteModule()
    {
      this.Execute();
    }

    protected override void Execute()
    {
      if (mustRecreateExecutionOrder)
      {
        RecreateExecutionOrder();
        mustRecreateExecutionOrder = false;
      }

      for (int i = 0; i < cachedOutputChannels.Count; i++)
      {
        cachedOutputChannels[i].ResetValue();
      }

      for (int i = 0; i < cachedExecutionActivities.Count; i++)
      {
        cachedExecutionActivities[i].ExecuteInternal();
      }
    }

    /// <summary>
    /// Calculates the execution order in which activities need to be executed so that for any activity, all previous activities on
    /// which it depends are executed. The execution order is a very slow process and is cached for performance.
    /// </summary>
    private void RecreateExecutionOrder()
    {
      cachedExecutionActivities.Clear();
      cachedOutputChannels.Clear();

      IEnumerable<FlowActivity> currentActivities = activities.Where(x => x.InputChannels.All(c => c.ConnectedOutputChannel == null));
      List<FlowActivity> tmpList = new List<FlowActivity>(currentActivities);

      while (true)
      {
        // Gatner next activities
        var nextActivities = currentActivities
          .SelectMany(x => x.OutputChannels)
          .SelectMany(x => x.ConnectedInputChannels)
          .Select(x => x.Owner)
          .Distinct();

        if (!nextActivities.Any())
          break;

        foreach (var activity in nextActivities)
        {
          // If already exists in list, remove from where it is and add it to the end
          if (tmpList.Contains(activity))
            tmpList.Remove(activity);

          tmpList.Add(activity);
        }

        currentActivities = nextActivities;
      }

      cachedExecutionActivities.AddRange(tmpList);

      cachedOutputChannels = cachedExecutionActivities
        .SelectMany(x => x.OutputChannels)
        .ToList();
    }
  }

  class InnerInputActivity : FlowActivity
  {
    private List<ChannelBridge> bridges = new List<ChannelBridge>();

    protected override void Execute()
    {
      // Transfer input values form the outside to the inside
      foreach (var bridge in bridges)
      {
        bridge.OutputChannel.SetValueAsObject(bridge.InputChannel.ValueAsObject);
      }
    }

    internal void BridgeOuterToInnerChannel(FlowInputChannel innerInputChannel, FlowInputChannel outerInputChannel)
    {
      var innerOutputChannel = InternalAddOutputChannel(outerInputChannel.Name, outerInputChannel.ValueType);
      innerOutputChannel.ConnectTo(innerInputChannel);

      var channelBridge = new ChannelBridge(outerInputChannel, innerOutputChannel);
      bridges.Add(channelBridge);
    }
  }
  class InnerOutputActivity : FlowActivity
  {
    private List<ChannelBridge> bridges = new List<ChannelBridge>();

    protected override void Execute()
    {
      foreach (var bridge in bridges)
      {
        bridge.OutputChannel.SetValueAsObject(bridge.InputChannel.ValueAsObject);
      }
    }

    internal void BridgeInnerToOuterChannel(FlowOutputChannel innerOutputChannel, FlowOutputChannel outerOutputChannel)
    {
      var innerInputChannel = InternalAddInputChannel(outerOutputChannel.Name, outerOutputChannel.ValueType);
      innerOutputChannel.ConnectTo(innerInputChannel);

      var channelBridge = new ChannelBridge(innerInputChannel, outerOutputChannel);
      bridges.Add(channelBridge);
    }
  }
  class ChannelBridge
  {
    public FlowInputChannel InputChannel { get; }
    public FlowOutputChannel OutputChannel { get; }

    public ChannelBridge(FlowInputChannel inputChannel, FlowOutputChannel outputChannel)
    {
      InputChannel = inputChannel;
      OutputChannel = outputChannel;
    }
  }
}
