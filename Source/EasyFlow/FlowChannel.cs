﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyFlow
{
    /// <summary>
    /// Channel represents an activity's input (where data comes into the activity) or output (through which data is sent out of the activity).
    /// </summary>
    public abstract class FlowChannel
    {
        public string Name { get; }
        public Type ValueType { get; }
        public FlowActivity Owner { get; }

        internal FlowChannel(string name, Type valueType, FlowActivity owner)
        {
            this.Name = name;
            this.ValueType = valueType;
            this.Owner = owner;
        }
    }
}
