﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Arrays
{
  public class ArrayConstantActivity : FlowActivity
  {
    public FlowOutputChannel<IEnumerable<object>?> OutChannel { get; }

    public IEnumerable<object>? Constant { get; set; }

    public ArrayConstantActivity()
    {
      OutChannel = InternalAddOutputChannel<IEnumerable<object>?>("Out");
    }

    protected override void Execute()
    {
      if (Constant != null)
        OutChannel.SetValue(Constant);
    }
  }
}
