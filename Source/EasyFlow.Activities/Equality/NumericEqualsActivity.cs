﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Equality
{
  public class NumericEqualsActivity : FlowActivity
  {
    public FlowInputChannel<double?> In1Channel { get; }
    public FlowInputChannel<double?> In2Channel { get; }
    public FlowOutputChannel<bool?> OutChannel { get; }

    public NumericEqualsActivity()
    {
      In1Channel = InternalAddInputChannel<double?>("In1");
      In2Channel = InternalAddInputChannel<double?>("In2");
      OutChannel = InternalAddOutputChannel<bool?>("Out");
    }

    protected override void Execute()
    {
      if (In1Channel.Value != null && In2Channel.Value != null)
        OutChannel.SetValue(In1Channel.Value == In2Channel.Value);
    }
  }
}
