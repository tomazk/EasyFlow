﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Equality
{
  public class NumericApproximateEqualsActivity : FlowActivity
  {
    public FlowInputChannel<double?> In1Channel { get; }
    public FlowInputChannel<double?> In2Channel { get; }
    public FlowInputChannel<double?> InMaxDifChannel { get; }
    public FlowOutputChannel<bool?> OutChannel { get; }

    public NumericApproximateEqualsActivity()
    {
      In1Channel = InternalAddInputChannel<double?>("In1");
      In2Channel = InternalAddInputChannel<double?>("In2");
      InMaxDifChannel = InternalAddInputChannel<double?>("InMaxDif");
      OutChannel = InternalAddOutputChannel<bool?>("Out");
    }

    protected override void Execute()
    {
      if (In1Channel.Value != null && In2Channel.Value != null && InMaxDifChannel.Value != null)
      {
        if (System.Math.Abs(In1Channel.Value.Value - In2Channel.Value.Value) < InMaxDifChannel.Value.Value)
          OutChannel.SetValue(true);
        else
          OutChannel.SetValue(false);
      }
    }
  }
}
