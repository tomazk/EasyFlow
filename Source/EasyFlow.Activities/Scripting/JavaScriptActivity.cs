﻿using Jint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyFlow.Serialization;
using EasyFlow.WorkflowEngine;

namespace EasyFlow.Activities.Scripting
{
  [ActivityType("Scripting.JavaScript")]
  public class JavaScriptActivity : FlowActivity
  {
    private Engine engine = new Engine();
    // Store only the channels added through JavaScriptActivity, but skip the other channels that may be added by the underlying FlowActivity.
    private List<FlowInputChannel> inputChannels = new List<FlowInputChannel>();
    private List<FlowOutputChannel> outputChannels = new List<FlowOutputChannel>();

    public string? Script { get; set; }

    public JavaScriptActivity()
    {
    }

    public FlowInputChannel<T> AddInputChannel<T>(string name)
    {
      var channel = InternalAddInputChannel<T>(name);
      inputChannels.Add(channel);
      return channel;
    }
    public FlowOutputChannel<T> AddOutputChannel<T>(string name)
    {
      var channel = InternalAddOutputChannel<T>(name);
      outputChannels.Add(channel);
      return channel;
    }

    protected override void Execute()
    {
      if (Script == null)
        return;

      foreach (var inputChannel in inputChannels)
      {
        engine.SetValue(inputChannel.Name, inputChannel.ValueAsObject);
      }

      engine.Execute(Script);

      foreach (var outputChannel in outputChannels)
      {
        var value = engine.GetValue(outputChannel.Name);

        if (value != null && !value.IsNull() && !value.IsUndefined())
          outputChannel.SetValueAsObject(value.ToObject());
      }
    }
  }
}
