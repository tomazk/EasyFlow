﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.SystemActivities
{
  public class ExceptionActivity : FlowActivity
  {
    public FlowInputChannel<ChannelSignal> InChannel { get; }

    public string? ErrorMessage { get; set; }

    public ExceptionActivity()
    {
      InChannel = InternalAddInputChannel<ChannelSignal>("In");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
        throw new Exception(ErrorMessage ?? "Exception occured in workflow.");
    }
  }
}
