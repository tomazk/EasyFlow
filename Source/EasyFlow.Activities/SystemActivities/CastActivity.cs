﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.SystemActivities
{
  public class CastActivity<T> : FlowActivity
  {
    public FlowInputChannel<object?> InChannel { get; }
    public FlowOutputChannel<T> OutChannel { get; }

    public CastActivity()
    {
      InChannel = InternalAddInputChannel<object?>("In");
      OutChannel = InternalAddOutputChannel<T>("Out");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
        OutChannel.SetValue((T)InChannel.Value);
    }
  }
}
