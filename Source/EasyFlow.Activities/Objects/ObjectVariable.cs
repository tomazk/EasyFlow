﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Objects
{
    public class FlowModuleVariableReader : FlowActivity
    {
        public FlowOutputChannel OutChannel { get; }

        public object? Value { get; set; }

        public FlowModuleVariableReader()
        {
            OutChannel = InternalAddOutputChannel("Out", typeof(object));
        }

        protected override void Execute()
        {
            if (Value != null)
                OutChannel.SetValueAsObject(Value);
        }
    }
}
