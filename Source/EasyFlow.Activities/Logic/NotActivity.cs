﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Logic
{
  public class NotActivity : FlowActivity
  {
    public FlowInputChannel<bool?> InChannel { get; }
    public FlowOutputChannel<bool?> OutChannel { get; }

    public NotActivity()
    {
      InChannel = InternalAddInputChannel<bool?>("In1");
      OutChannel = InternalAddOutputChannel<bool?>("Out");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
        OutChannel.SetValue(!InChannel.Value.Value);
    }
  }
}
