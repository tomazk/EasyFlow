﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Logic
{
  public class OrActivity : FlowActivity
  {
    public FlowInputChannel<bool?> In1Channel { get; }
    public FlowInputChannel<bool?> In2Channel { get; }
    public FlowOutputChannel<bool?> OutChannel { get; }

    public OrActivity()
    {
      In1Channel = InternalAddInputChannel<bool?>("In1");
      In2Channel = InternalAddInputChannel<bool?>("In2");
      OutChannel = InternalAddOutputChannel<bool?>("Out");
    }

    protected override void Execute()
    {
      if (In1Channel.Value != null && In2Channel.Value != null)
        OutChannel.SetValue(In1Channel.Value.Value || In2Channel.Value.Value);
    }
  }
}
