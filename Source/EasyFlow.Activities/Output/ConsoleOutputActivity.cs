﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyFlow.Serialization;
using EasyFlow.WorkflowEngine;

namespace EasyFlow.Activities.Output
{
  public class ConsoleOutputActivity : FlowActivity
  {
    public FlowInputChannel<object?> InChannel { get; }

    public string? Prefix { get; set; }
    public string? Suffix { get; set; }

    public ConsoleOutputActivity()
    {
      InChannel = InternalAddInputChannel<object?>("In");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
        Console.WriteLine(Prefix + JsonConvert.SerializeObject(InChannel.Value, Formatting.Indented) + Suffix);
    }
  }
}
