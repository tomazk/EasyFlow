﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Conditional
{
  public class IfActivity : FlowActivity
  {
    public FlowInputChannel<bool?> InChannel { get; }
    public FlowOutputChannel<ChannelSignal?> OutYesChannel { get; }
    public FlowOutputChannel<ChannelSignal?> OutNoChannel { get; }

    public IfActivity()
    {
      InChannel = InternalAddInputChannel<bool?>("In");
      OutYesChannel = InternalAddOutputChannel<ChannelSignal?>("OutTrue");
      OutNoChannel = InternalAddOutputChannel<ChannelSignal?>("OutFalse");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
      {
        if (InChannel.Value == true)
          OutYesChannel.SetValue(ChannelSignal.Value);
        else
          OutNoChannel.SetValue(ChannelSignal.Value);
      }
    }
  }
}
