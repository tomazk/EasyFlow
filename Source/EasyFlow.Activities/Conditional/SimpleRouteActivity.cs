﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Conditional
{
  /// <summary>
  /// Gate will send input value to output only when input bool value is set to true.
  /// </summary>
  public class SimpleRouteActivity : FlowActivity
  {
    public FlowInputChannel<object?> InChannel { get; }
    public FlowInputChannel<bool?> InBoolChannel { get; }
    public FlowOutputChannel<object?> OutTrueChannel { get; }
    public FlowOutputChannel<object?> OutFalseChannel { get; }

    public SimpleRouteActivity()
    {
      InChannel = InternalAddInputChannel<object?>("In");
      InBoolChannel = InternalAddInputChannel<bool?>("InBool");
      OutTrueChannel = InternalAddOutputChannel<object?>("OutTrue");
      OutFalseChannel = InternalAddOutputChannel<object?>("OutFalse");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null && InBoolChannel.Value != null)
      {
        if (InBoolChannel.Value == true)
          OutTrueChannel.SetValue(InChannel.Value);
        else
          OutFalseChannel.SetValue(InChannel.Value);
      }
    }
  }
}
