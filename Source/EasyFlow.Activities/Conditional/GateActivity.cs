﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Conditional
{
  /// <summary>
  /// Gate will send input value to output only when input bool value is set to true.
  /// </summary>
  public class GateActivity : FlowActivity
  {
    public FlowInputChannel<object?> InChannel { get; }
    public FlowInputChannel<bool?> InBoolChannel { get; }
    public FlowOutputChannel<object?> OutChannel { get; }

    public GateActivity()
    {
      InChannel = InternalAddInputChannel<object?>("In");
      InBoolChannel = InternalAddInputChannel<bool?>("InBool");
      OutChannel = InternalAddOutputChannel<object?>("Out");
    }

    protected override void Execute()
    {
      if (InBoolChannel.Value == true)
        OutChannel.SetValue(InChannel.Value);
    }
  }
}
