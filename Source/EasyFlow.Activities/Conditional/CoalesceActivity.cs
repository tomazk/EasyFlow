﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Conditional
{
  public class CoalesceActivity : FlowActivity
  {
    public FlowOutputChannel OutChannel { get; }

    public CoalesceActivity()
    {
      OutChannel = InternalAddOutputChannel("Out", typeof(object));
    }

    public FlowInputChannel AddInputChannel(string name)
    {
      return InternalAddInputChannel($"In{InputChannels.Count()}", typeof(object));
    }

    protected override void Execute()
    {
      var channel = InputChannels.FirstOrDefault(x => x.ValueAsObject != null);

      if (channel != null)
        OutChannel.SetValueAsObject(channel.ValueAsObject);
    }
  }
}
