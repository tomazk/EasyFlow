﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Loops
{
  public class ForeachActivity : FlowActivity
  {
    private FlowModule module;
    private FlowModuleVariableReader innerItemActivity;
    private FlowModuleVariableWriter innerOutputActivity;
    private FlowModuleVariableReader innerPrevOutputActivity;
    private LoopIndexActivity loopIndexActivity;

    public FlowInputChannel<IEnumerable<object?>?> InChannel { get; }
    public FlowOutputChannel<IEnumerable<object?>?> OutChannel { get; }

    public FlowOutputChannel InnerItemChannel { get; }
    public FlowInputChannel InnerOutputChannel { get; }
    public FlowOutputChannel InnerPrevOutputChannel { get; }
    public FlowOutputChannel<int?> InnerLoopIndexChannel { get; }

    public ForeachActivity(IActivityFactory activityProvider)
    {
      module = new FlowModule(activityProvider);
      InChannel = InternalAddInputChannel<IEnumerable<object?>?>("In");
      OutChannel = InternalAddOutputChannel<IEnumerable<object?>?>("Out");

      // Init inner part
      innerItemActivity = module.AddActivity<FlowModuleVariableReader>();
      innerOutputActivity = module.AddActivity<FlowModuleVariableWriter>();
      innerPrevOutputActivity = module.AddActivity<FlowModuleVariableReader>();
      loopIndexActivity = module.AddActivity<LoopIndexActivity>();

      InnerItemChannel = innerItemActivity.OutChannel;
      InnerOutputChannel = innerOutputActivity.InChannel;
      InnerPrevOutputChannel = innerPrevOutputActivity.OutChannel;
      InnerLoopIndexChannel = loopIndexActivity.OutChannel;
    }

    public T AddActivity<T>(Action<T>? setup = null) where T : FlowActivity
    {
      return module.AddActivity(setup);
    }

    public FlowActivity AddActivity(Type activityType, Action<FlowActivity>? setup = null)
    {
      return module.AddActivity(activityType, setup);
    }

    protected override void Execute()
    {
      // Loop through all items in input array

      if (InChannel.Value == null)
        return;

      List<object?> outItems = new List<object?>(InChannel.Value.Count());
      int index = 0;

      foreach (var item in InChannel.Value)
      {
        innerItemActivity.Value = item;
        loopIndexActivity.Index = index;

        // Execute the base module code
        module.ExecuteModule();

        outItems.Add(innerOutputActivity.Value);
        innerPrevOutputActivity.Value = innerOutputActivity.Value;

        index++;
      }

      OutChannel.SetValue(outItems);
    }
  }

  public class LoopIndexActivity : FlowActivity
  {
    public FlowOutputChannel<int?> OutChannel { get; }

    public int? Index { get; set; }

    public LoopIndexActivity()
    {
      OutChannel = InternalAddOutputChannel<int?>("Out");
    }

    protected override void Execute()
    {
      if (Index != null)
        OutChannel.SetValue(Index.Value);
    }
  }
}
