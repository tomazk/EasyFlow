﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Loops
{
  public class ForActivity : FlowActivity
  {
    public FlowInputChannel<IEnumerable<object>?> InChannel { get; }
    public FlowOutputChannel<IEnumerable<object>?> OutChannel { get; }

    public ForActivity()
    {
      InChannel = InternalAddInputChannel<IEnumerable<object>?>("In");
      OutChannel = InternalAddOutputChannel<IEnumerable<object>?>("Out");
    }

    protected override void Execute()
    {
      
    }
  }
}
