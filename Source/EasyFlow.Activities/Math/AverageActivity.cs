﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class AverageActivity : FlowActivity
  {
    private List<FlowInputChannel<double?>> inChannels = new List<FlowInputChannel<double?>>();

    public FlowOutputChannel<double?> OutChannel { get; }

    public AverageActivity()
    {
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      if (inChannels.All(x => x.Value != null))
      {
        var sum = inChannels
          .Select(x => x.Value)
          .Aggregate((a, b) => a + b);
        OutChannel.SetValue(sum / (double)inChannels.Count);
      }
    }

    public FlowInputChannel<double?> AddInputChannel()
    {
      var inChannel = InternalAddInputChannel<double?>($"In{inChannels.Count}");
      inChannels.Add(inChannel);
      return inChannel;
    }
  }
}
