﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class PowerActivity : FlowActivity
  {
    public FlowInputChannel<double?> InChannel { get; }
    public FlowInputChannel<double?> InPowerChannel { get; }
    public FlowOutputChannel<double?> OutChannel { get; }

    public PowerActivity()
    {
      InChannel = InternalAddInputChannel<double?>("In");
      InPowerChannel = InternalAddInputChannel<double?>("InExponent");
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
      {
        double power = InPowerChannel.Value != null ? InPowerChannel.Value.Value : 1d;
        OutChannel.SetValue(System.Math.Pow(InChannel.Value.Value, power));
      }
    }
  }
}
