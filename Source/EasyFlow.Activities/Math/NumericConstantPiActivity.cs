﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class NumericConstantPiActivity : FlowActivity
  {
    public FlowOutputChannel<double?> OutChannel { get; }

    public NumericConstantPiActivity()
    {
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      OutChannel.SetValue(System.Math.PI);
    }
  }
}
