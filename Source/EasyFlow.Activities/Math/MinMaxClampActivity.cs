﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class MinMaxClampActivity : FlowActivity
  {
    public FlowInputChannel<double?> InChannel { get; }
    public FlowInputChannel<double?> InMinChannel { get; }
    public FlowInputChannel<double?> InMaxChannel { get; }
    public FlowOutputChannel<double?> OutChannel { get; }

    public MinMaxClampActivity()
    {
      InChannel = InternalAddInputChannel<double?>("In");
      InMinChannel = InternalAddInputChannel<double?>("InMin");
      InMaxChannel = InternalAddInputChannel<double?>("InMax");
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
      {
        var min = InMinChannel.Value != null ? InMinChannel.Value.Value : InChannel.Value.Value;
        var max = InMaxChannel.Value != null ? InMaxChannel.Value.Value : InChannel.Value.Value;

        OutChannel.SetValue(System.Math.Clamp(InChannel.Value.Value, min, max));
      }
    }
  }
}
