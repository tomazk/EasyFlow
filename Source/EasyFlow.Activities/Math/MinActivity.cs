﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class MinActivity : FlowActivity
  {
    public FlowInputChannel<double?> In1Channel { get; }
    public FlowInputChannel<double?> In2Channel { get; }
    public FlowOutputChannel<double?> OutChannel { get; }

    public MinActivity()
    {
      In1Channel = InternalAddInputChannel<double?>("In1");
      In2Channel = InternalAddInputChannel<double?>("In2");
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      if (In1Channel.Value != null && In2Channel.Value != null)
      {
        if (In1Channel.Value.Value <= In2Channel.Value.Value)
          OutChannel.SetValue(In1Channel.Value.Value);
        else
          OutChannel.SetValue(In2Channel.Value.Value);
      }
    }
  }
}
