﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class NumericConstantActivity : FlowActivity
  {
    public FlowOutputChannel<double?> OutChannel { get; }

    public double? Value { get; set; }

    public NumericConstantActivity()
    {
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      if (Value != null)
        OutChannel.SetValue(Value.Value);
    }
  }
}
