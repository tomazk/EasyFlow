﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class RootActivity : FlowActivity
  {
    public FlowInputChannel<double?> InChannel { get; }
    public FlowInputChannel<double?> InRootChannel { get; }
    public FlowOutputChannel<double?> OutChannel { get; }

    public RootActivity()
    {
      InChannel = InternalAddInputChannel<double?>("In");
      InRootChannel = InternalAddInputChannel<double?>("InRoot");
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
      {
        double root = InRootChannel.Value != null ? InRootChannel.Value.Value : 1d;

        if (root > 0)
          OutChannel.SetValue(System.Math.Pow(InChannel.Value.Value, 1 / root));
      }
    }
  }
}
