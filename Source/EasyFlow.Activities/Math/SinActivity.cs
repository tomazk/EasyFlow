﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyFlow.Activities.Math
{
  public class SinActivity : FlowActivity
  {
    public FlowInputChannel<double?> InChannel { get; }
    public FlowOutputChannel<double?> OutChannel { get; }

    public SinActivity()
    {
      InChannel = InternalAddInputChannel<double?>("In");
      OutChannel = InternalAddOutputChannel<double?>("Out");
    }

    protected override void Execute()
    {
      if (InChannel.Value != null)
        OutChannel.SetValue(System.Math.Sin(InChannel.Value.Value));
    }
  }
}
